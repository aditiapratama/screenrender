# ScreenRender

ScreenRender allows you to capture the Blender UI for each frame of an animation.
This enables you to neatly show how the UI and different values are changing during animation.

It also allows you to hide the animation highlight (yellow/green) on animated fields, or override it with another color, to make value changes seem like user interactions.

[Get the latest version here](https://gitlab.com/LucaRood/screenrender/-/releases)

A setup like this:
![Blender scene](media/Demo.png)

Allows you to capture a clip like this:
![ScreenRender result](media/Demo.mp4)
